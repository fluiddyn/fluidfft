# fluiddyn

This note is kept for historical reasons as the
[development of project has moved to a different forge](https://fluiddyn.readthedocs.io/en/latest/advice_developers.html).

- Source code: <https://foss.heptapod.net/fluiddyn/fluidfft>
- Documentation: <https://fluidfft.readthedocs.io/>

We continue to publish releases installable using `pip` or `conda`. For further details, check the documentation, under "Installation".